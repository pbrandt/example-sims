%General Mission Analysis Tool(GMAT) Script
%Created: 2020-05-22 09:21:15


%----------------------------------------
%---------- Spacecraft
%----------------------------------------

Create Spacecraft Gateway;
GMAT Gateway.DateFormat = UTCGregorian;
GMAT Gateway.Epoch = '01 May 2024 00:00:00.000';
GMAT Gateway.CoordinateSystem = EarthMJ2000Eq;
GMAT Gateway.DisplayStateType = ModifiedKeplerian;
GMAT Gateway.RadPer = 6778.000000000005;
GMAT Gateway.RadApo = 66377.99999999975;
GMAT Gateway.INC = 28.00000000000002;
GMAT Gateway.RAAN = 130;
GMAT Gateway.AOP = 180;
GMAT Gateway.TA = 360;
GMAT Gateway.DryMass = 13000;
GMAT Gateway.Cd = 2.2;
GMAT Gateway.Cr = 1.1;
GMAT Gateway.DragArea = 246;
GMAT Gateway.SRPArea = 246;
GMAT Gateway.Tanks = {XenonTank};
GMAT Gateway.Thrusters = {HallThrusters};
GMAT Gateway.PowerSystem = ROSA;
GMAT Gateway.NAIFId = -10000001;
GMAT Gateway.NAIFIdReferenceFrame = -9000001;
GMAT Gateway.OrbitColor = Red;
GMAT Gateway.TargetColor = Teal;
GMAT Gateway.OrbitErrorCovariance = [ 1e+70 0 0 0 0 0 ; 0 1e+70 0 0 0 0 ; 0 0 1e+70 0 0 0 ; 0 0 0 1e+70 0 0 ; 0 0 0 0 1e+70 0 ; 0 0 0 0 0 1e+70 ];
GMAT Gateway.CdSigma = 1e+70;
GMAT Gateway.CrSigma = 1e+70;
GMAT Gateway.Id = 'SatId';
GMAT Gateway.Attitude = CoordinateSystemFixed;
GMAT Gateway.SPADSRPScaleFactor = 1;
GMAT Gateway.ModelFile = 'aura.3ds';
GMAT Gateway.ModelOffsetX = 0;
GMAT Gateway.ModelOffsetY = 0;
GMAT Gateway.ModelOffsetZ = 0;
GMAT Gateway.ModelRotationX = 0;
GMAT Gateway.ModelRotationY = 0;
GMAT Gateway.ModelRotationZ = 0;
GMAT Gateway.ModelScale = 1;
GMAT Gateway.AttitudeDisplayStateType = 'Quaternion';
GMAT Gateway.AttitudeRateDisplayStateType = 'AngularVelocity';
GMAT Gateway.AttitudeCoordinateSystem = EarthMJ2000Eq;
GMAT Gateway.EulerAngleSequence = '321';

%----------------------------------------
%---------- Hardware Components
%----------------------------------------

Create ElectricThruster HallThrusters;
GMAT HallThrusters.CoordinateSystem = Local;
GMAT HallThrusters.Origin = Earth;
GMAT HallThrusters.Axes = VNB;
GMAT HallThrusters.ThrustDirection1 = 1;
GMAT HallThrusters.ThrustDirection2 = 0;
GMAT HallThrusters.ThrustDirection3 = 0;
GMAT HallThrusters.DutyCycle = 1;
GMAT HallThrusters.ThrustScaleFactor = 1;
GMAT HallThrusters.DecrementMass = true;
GMAT HallThrusters.Tank = {XenonTank};
GMAT HallThrusters.MixRatio = [ 1 ];
GMAT HallThrusters.GravitationalAccel = 9.806649999999999;
GMAT HallThrusters.ThrustModel = ConstantThrustAndIsp;
GMAT HallThrusters.MaximumUsablePower = 70;
GMAT HallThrusters.MinimumUsablePower = 0.638;
GMAT HallThrusters.ThrustCoeff1 = -5.19082;
GMAT HallThrusters.ThrustCoeff2 = 2.96519;
GMAT HallThrusters.ThrustCoeff3 = -14.4789;
GMAT HallThrusters.ThrustCoeff4 = 54.05382;
GMAT HallThrusters.ThrustCoeff5 = -0.00100092;
GMAT HallThrusters.MassFlowCoeff1 = -0.004776;
GMAT HallThrusters.MassFlowCoeff2 = 0.05717;
GMAT HallThrusters.MassFlowCoeff3 = -0.09956;
GMAT HallThrusters.MassFlowCoeff4 = 0.03211;
GMAT HallThrusters.MassFlowCoeff5 = 2.13781;
GMAT HallThrusters.FixedEfficiency = 0.7;
GMAT HallThrusters.Isp = 2600;
GMAT HallThrusters.ConstantThrust = 1.77;

Create ElectricTank XenonTank;
GMAT XenonTank.AllowNegativeFuelMass = false;
GMAT XenonTank.FuelMass = 5000;

Create SolarPowerSystem ROSA;
GMAT ROSA.EpochFormat = 'UTCGregorian';
GMAT ROSA.InitialEpoch = ''01 Jan 2000 11:59:28.000'';
GMAT ROSA.InitialMaxPower = 70000;
GMAT ROSA.AnnualDecayRate = 5;
GMAT ROSA.Margin = 5;
GMAT ROSA.BusCoeff1 = 0.3;
GMAT ROSA.BusCoeff2 = 0;
GMAT ROSA.BusCoeff3 = 0;
GMAT ROSA.ShadowModel = 'None';
GMAT ROSA.ShadowBodies = {'Earth'};
GMAT ROSA.SolarCoeff1 = 1.32077;
GMAT ROSA.SolarCoeff2 = -0.10848;
GMAT ROSA.SolarCoeff3 = -0.11665;
GMAT ROSA.SolarCoeff4 = 0.10843;
GMAT ROSA.SolarCoeff5 = -0.01279;



%----------------------------------------
%---------- ForceModels
%----------------------------------------

Create ForceModel DefaultProp_ForceModel;
GMAT DefaultProp_ForceModel.CentralBody = Earth;
GMAT DefaultProp_ForceModel.PrimaryBodies = {Earth};
GMAT DefaultProp_ForceModel.PointMasses = {Jupiter, Luna, Sun};
GMAT DefaultProp_ForceModel.Drag = None;
GMAT DefaultProp_ForceModel.SRP = On;
GMAT DefaultProp_ForceModel.RelativisticCorrection = Off;
GMAT DefaultProp_ForceModel.ErrorControl = RSSStep;
GMAT DefaultProp_ForceModel.GravityField.Earth.Degree = 10;
GMAT DefaultProp_ForceModel.GravityField.Earth.Order = 10;
GMAT DefaultProp_ForceModel.GravityField.Earth.StmLimit = 100;
GMAT DefaultProp_ForceModel.GravityField.Earth.PotentialFile = 'JGM3.cof';
GMAT DefaultProp_ForceModel.GravityField.Earth.TideModel = 'None';
GMAT DefaultProp_ForceModel.SRP.Flux = 1367;
GMAT DefaultProp_ForceModel.SRP.SRPModel = Spherical;
GMAT DefaultProp_ForceModel.SRP.Nominal_Sun = 149597870.691;

%----------------------------------------
%---------- Propagators
%----------------------------------------

Create Propagator DefaultProp;
GMAT DefaultProp.FM = DefaultProp_ForceModel;
GMAT DefaultProp.Type = RungeKutta89;
GMAT DefaultProp.InitialStepSize = 60;
GMAT DefaultProp.Accuracy = 9.999999999999999e-12;
GMAT DefaultProp.MinStep = 0.001;
GMAT DefaultProp.MaxStep = 2700;
GMAT DefaultProp.MaxStepAttempts = 50;
GMAT DefaultProp.StopIfAccuracyIsViolated = true;

%----------------------------------------
%---------- Burns
%----------------------------------------

Create FiniteBurn VelocityFiniteBurn;
GMAT VelocityFiniteBurn.Thrusters = {HallThrusters};
GMAT VelocityFiniteBurn.ThrottleLogicAlgorithm = 'MaxNumberOfThrusters';

%----------------------------------------
%---------- Subscribers
%----------------------------------------

Create OrbitView DefaultOrbitView;
GMAT DefaultOrbitView.SolverIterations = Current;
GMAT DefaultOrbitView.UpperLeft = [ 0.1446428571428572 0.03755868544600939 ];
GMAT DefaultOrbitView.Size = [ 0.9964285714285714 0.9589201877934272 ];
GMAT DefaultOrbitView.RelativeZOrder = 137;
GMAT DefaultOrbitView.Maximized = true;
GMAT DefaultOrbitView.Add = {Gateway, Earth};
GMAT DefaultOrbitView.CoordinateSystem = EarthMJ2000Eq;
GMAT DefaultOrbitView.DrawObject = [ true true ];
GMAT DefaultOrbitView.DataCollectFrequency = 1;
GMAT DefaultOrbitView.UpdatePlotFrequency = 50;
GMAT DefaultOrbitView.NumPointsToRedraw = 0;
GMAT DefaultOrbitView.ShowPlot = true;
GMAT DefaultOrbitView.MaxPlotPoints = 20000;
GMAT DefaultOrbitView.ShowLabels = true;
GMAT DefaultOrbitView.ViewPointReference = Earth;
GMAT DefaultOrbitView.ViewPointVector = [ 30000 0 0 ];
GMAT DefaultOrbitView.ViewDirection = Earth;
GMAT DefaultOrbitView.ViewScaleFactor = 1;
GMAT DefaultOrbitView.ViewUpCoordinateSystem = EarthMJ2000Eq;
GMAT DefaultOrbitView.ViewUpAxis = Z;
GMAT DefaultOrbitView.EclipticPlane = Off;
GMAT DefaultOrbitView.XYPlane = On;
GMAT DefaultOrbitView.WireFrame = Off;
GMAT DefaultOrbitView.Axes = On;
GMAT DefaultOrbitView.Grid = Off;
GMAT DefaultOrbitView.SunLine = Off;
GMAT DefaultOrbitView.UseInitialView = On;
GMAT DefaultOrbitView.StarCount = 7000;
GMAT DefaultOrbitView.EnableStars = On;
GMAT DefaultOrbitView.EnableConstellations = Off;

Create EphemerisFile EphemerisFile1;
GMAT EphemerisFile1.UpperLeft = [ 0 0 ];
GMAT EphemerisFile1.Size = [ 0 0 ];
GMAT EphemerisFile1.RelativeZOrder = 0;
GMAT EphemerisFile1.Maximized = false;
GMAT EphemerisFile1.Spacecraft = Gateway;
GMAT EphemerisFile1.Filename = './gmat-eor-truth.e';
GMAT EphemerisFile1.FileFormat = STK-TimePosVel;
GMAT EphemerisFile1.EpochFormat = UTCGregorian;
GMAT EphemerisFile1.InitialEpoch = InitialSpacecraftEpoch;
GMAT EphemerisFile1.FinalEpoch = FinalSpacecraftEpoch;
GMAT EphemerisFile1.StepSize = 300;
GMAT EphemerisFile1.Interpolator = Lagrange;
GMAT EphemerisFile1.InterpolationOrder = 7;
GMAT EphemerisFile1.CoordinateSystem = EarthMJ2000Eq;
GMAT EphemerisFile1.OutputFormat = LittleEndian;
GMAT EphemerisFile1.WriteEphemeris = true;
GMAT EphemerisFile1.DistanceUnit = Kilometers;
GMAT EphemerisFile1.IncludeEventBoundaries = false;


%----------------------------------------
%---------- Mission Sequence
%----------------------------------------

BeginMissionSequence;
BeginFiniteBurn VelocityFiniteBurn(Gateway);
Propagate DefaultProp(Gateway) {Gateway.ElapsedDays = 100};
