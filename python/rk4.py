

def rk4_step(x, fx, t, dt):
    """takes a single step of the runge kutta 4th order integrator"""
    k1 = fx(x, t)
    k2 = fx(x + k1 * dt/2, t + dt/2)
    k3 = fx(x + k2 * dt/2, t + dt/2)
    k4 = fx(x + k3 * dt, t + dt)
    return x + (k1 + k4) * dt / 6 + (k2 + k3) * dt / 3
