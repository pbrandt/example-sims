import math
import numpy as np

import rk4

MU_EARTH = 3.9860043543609598E+05 # gravitational parameter from de431
J2_EARTH = 17555283226.38659  # un-normalized jgm-3 km
ISP = 2600
THRUST_KN = 1.77 / 1e3
G0 = 9.80665
MDOT = THRUST_KN * 1e3 / G0 / ISP

def state_deriv(x, t):
    """ computes the derivative of the spacecraft """
    r = np.linalg.norm(x[0:3])

    # point mass earth
    grav_acc = -1 * x[0:3] * MU_EARTH / (r**3)

    # add j2 term
    scale = J2_EARTH / math.pow(r, 7)
    x2y2 = x[0]*x[0] + x[1]*x[1]
    z2 = x[2]*x[2]
    j2_acc = np.array([
        scale * x[0] * (6.0 * z2 - 1.5 * x2y2),
        scale * x[1] * (6.0 * z2 - 1.5 * x2y2),
        scale * x[2] * (3.0 * z2 - 4.5 * x2y2),
    ])

    # electric thrust
    thrust_acc = x[3:6] / np.linalg.norm(x[3:6]) * THRUST_KN / x[6]

    xdot = np.zeros(7)
    xdot[0:3] = x[3:6]
    xdot[3:6] = grav_acc + j2_acc + thrust_acc
    xdot[6] = -1 * MDOT

    return xdot


def main():
    x = np.array([
        4356.814418455364, # pos
        -5192.249235460436,
        1.169076583991219e-12,
        6.98729537277121, # vel
        5.863036970219034,
        -4.849863326963697,
        18000 # mass
    ])

    t = 0
    tf = 100 * 86400
    dt = 1
    log_interval = 300
    next_log = 0
    with open('log.csv', 'w') as f:
        f.write("t {s}, x {km}, y {km}, z {km}, vx {km/s}, vy {km/s}, vz {km/s}, m {kg}, met {day}, r {km}\n")

        def write_log(x, t):
            f.write(f'{t}, {", ".join([str(v) for v in x])}, {t/86400}, {np.linalg.norm(x[0:3])}\n')

        # run rk4
        while t < tf:
            if t >= next_log:
                write_log(x, t)
                next_log = next_log + log_interval
            
            x = rk4.rk4_step(x, state_deriv, t, dt)
            t = t + dt
        write_log(x, t)
    
if __name__ == "__main__":
    main()
