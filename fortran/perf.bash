#!/usr/bin/env bash

set -e

# make reals and doubles both 64 bits
FLAGS='-Wall -Wextra -Wsurprising -Werror'

gfortran $FLAGS -c -shared vec.f90
gfortran $FLAGS -c -shared integrators.f90
gfortran $FLAGS -o eor-perf eor.f90 vec.o integrators.o


# start process
rm log.csv
./eor-perf &

# record performance data
perf record -F 99 -p $! -g &
PERF_PID=$!
sleep 2
kill $PERF_PID
sleep 1
perf script > out.perf

# print flame chart
FLAME_HOME='/home/pete/code/FlameGraph'
$FLAME_HOME/stackcollapse-perf.pl out.perf > out.folded
$FLAME_HOME/flamegraph.pl out.folded > flame.svg
