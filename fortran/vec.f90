module vec
implicit none
private
public :: cross, unit, unit3

contains
    pure function cross(u, v) result(x)
        ! standard cross product
        real*8, intent(in) :: u(3), v(3)
        real*8 :: x(3)

        x(1) = u(2) * v(3) - u(3) * v(2)
        x(2) = u(3) * v(1) - u(1) * v(3)
        x(3) = u(1) * v(2) - u(2) * v(1)
    end function cross

    pure function unit(v) result(y)
        ! unit vector
        ! motivated to write this because ONE TIME i accidentlaly did this:
        ! mistake: v(4:6) / norm2(v(3:6))
        ! SEE, i accidentlaly took the norm of a 4-vector. bad.
        ! so this function is to prevent mistakes
        real*8, intent(in) :: v(:)
        real*8 :: y(size(v))
        y = v / norm2(v)
    end function unit

    pure function unit3(v) result(y)
        ! unit vector for 3 dimension vector
        real*8, intent(in) :: v(3)
        real*8 :: y(3)
        y = v / norm2(v)
    end function unit3
end module vec
