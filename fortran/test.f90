program test
    implicit none
    real :: u(3), v(3)

    u = [1, 2, 3]
    v = [4, 5, 6]

    v(2:3) = u(1:2)

    print *, 'hello world', u, v
end program test