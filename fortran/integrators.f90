module integrators
    implicit none
    private
    public :: rk4, f

    interface
        function f(x, t) result(dx)
        ! Derivative of a state vector x
            real*8, intent(in), dimension(7) :: x
            real*8, intent(in) :: t
            real*8 :: dx(7)
        end function f
    end interface

    contains

    function rk4(x, t, dt, fx) result(y)
    ! integrates one timestep. arbitrary vector length. all memory on the stack bitches.
        real*8, intent(in) :: x(7), t, dt
        procedure(f) :: fx
        real*8 :: y(7)
        real*8, dimension(7) :: k1, k2, k3, k4

        k1 = fx(x, t)
        k2 = fx(x + dt * 0.5 * k1, t + dt * 0.5)
        k3 = fx(x + dt * 0.5 * k2, t + dt * 0.5)
        k4 = fx(x + dt * k3, t + dt)

        y = x &
            + 1.0 / 6.0 * dt * k1 &
            + 1.0 / 3.0 * dt * k2 &
            + 1.0 / 3.0 * dt * k3 &
            + 1.0 / 6.0 * dt * k4
    end function rk4

end module integrators