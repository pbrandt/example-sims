module constants
    implicit none
    real*8, parameter :: mu_earth = 3.9860043543609598E+05 ! from de431
    real*8, parameter :: j2_earth = 17555283226.38659 ! un-normalized jgm-3 km
    real*8, parameter :: isp = 2600.0
    real*8, parameter :: thrust = 1.77 ! newtons
    real*8, parameter :: g0 = 9.80665
    real*8, parameter :: mdot = thrust / isp / g0
end module constants


program eor
    use vec
    use integrators
    implicit none

    real*8 :: x(7), t, t_log
    real*8, parameter :: tf = 86400.0 * 100.0 ! 100 days
    real*8, parameter :: dt = 1.0 ! s
    real*8, parameter :: log_rate = 300.0 ! s
    procedure(f) :: eor_fx

    x = [ 4356.814418455364, -5192.249235460436, 1.169076583991219e-12, &
          6.98729537277121, 5.863036970219034, -4.849863326963697, &
          18000.0 ]
    t = 0.0
    t_log = 0.0

    ! print *, 'starting x ', x
    ! print *, 'starting dx', eor_fx(x, 0d0)
    ! print *, 'one step   ', rk4(x, t, dt, eor_fx)


    ! print *, "starting simulation"
    open(1, file = "log.csv", status="new")
    write(1, '(*(G0.6,:,","))') "t {s}, x {km}, y {km}, z {km}, vx {km/s}, vy {km/s}, vz {km/s}, m {kg}, met {day}, r {km}"

    do while (t < tf)
        ! log every 5 minutes
        if (t >= t_log ) then
            write(1, '(*(G0.6,:,","))') t, x, t / 86400.0, norm2(x(1:3))
            t_log = t_log + log_rate
        end if
        x = rk4(x, t, dt, eor_fx)
        t = t + dt
    end do
    write(1, '(*(G0.6,:,","))') t, x, t / 86400.0, norm2(x(1:3))

end program



function eor_fx(x, t) result(dx)
    ! calculate the derivatives for the electric thrusting spacecraft
    ! spacecraft thrusts exactly along the velocity direction
    use vec
    use constants
    implicit none
    real*8, intent(in) :: x(7), t
    real*8 :: dx(7), r_mag, gravity_acc(3), j2_acc(3), thrust_acc(3), r7, scale, x2y2, z2

    ! velocity is derivative of position
    dx(1:3) = x(4:6)

    ! earth gravity point mass
    r_mag = norm2(x(1:3))
    gravity_acc = - mu_earth / r_mag / r_mag * unit3(x(1:3))

    ! earth j2
    r7 = r_mag ** 7
    scale = j2_earth / r7
    x2y2 = x(1)*x(1) + x(2)*x(2)
    z2 = x(3)*x(3)
    j2_acc(1) = scale * x(1) * (6.0 * z2 - 1.5 * x2y2)
    j2_acc(2) = scale * x(2) * (6.0 * z2 - 1.5 * x2y2)
    j2_acc(3) = scale * x(3) * (3.0 * z2 - 4.5 * x2y2)

    ! spacecraft solar electric propulsion thrust
    thrust_acc = thrust / 1.0e3 / x(7) * unit3(x(4:6))

    ! combine accelerations
    dx(4:6) = gravity_acc + j2_acc + thrust_acc

    ! mdot
    dx(7) = - mdot

    ! pretend to use t in order to suppress the "unused variable" error
    ! hopefully this gets optimized away
    r_mag = t

end function