#!/usr/bin/env bash

set -e

FLAGS='-Wall -Wextra -Wsurprising -Werror'
gfortran $FLAGS -c -shared vec.f90
gfortran $FLAGS -c -shared integrators.f90
gfortran $FLAGS -o eor eor.f90 vec.o integrators.o

rm -f log.csv
echo "gfortran -O0"
time ./eor

FLAGS='-Wall -Wextra -Wsurprising -Werror -Ofast'
gfortran $FLAGS -c -shared vec.f90
gfortran $FLAGS -c -shared integrators.f90
gfortran $FLAGS -o eor eor.f90 vec.o integrators.o

rm log.csv
echo "gfortran -Ofast"
time ./eor


# intel
FLAGS='-O0'
ifort $FLAGS -c -shared vec.f90
ifort $FLAGS -c -shared integrators.f90
ifort $FLAGS -o eor eor.f90 vec.o integrators.o

rm log.csv
echo "ifort -O0"
time ./eor

FLAGS='-O3'
ifort $FLAGS -c -shared vec.f90
ifort $FLAGS -c -shared integrators.f90
ifort $FLAGS -o eor eor.f90 vec.o integrators.o

rm log.csv
echo "ifort -O3"
time ./eor


gnuplot plot.gp
