mod integrators;

fn main() {
    println!("Rust EOR Sim");
    let mut x: [f64; 7] = [4356.814418455364, -5192.249235460436, 1.169076583991219e-12, 6.98729537277121, 5.863036970219034, -4.849863326963697, 18000.0];
    let mut t: f64 = 0.0;
    let tf: f64 = 86400.0 * 100.0;
    let dt: f64 = 1.0;
    let mut next_day: f64 = 0.0;
    let mut next_log: f64 = 0.0;

    // create log file

    // integration loop
    while t < tf {
        // if t > next_log {
        // }

        if t >= next_day {
            println!("Day {}", (t / 86400.0) as i32);
            next_day += 86400.0
        }

        // integrate state
        x[0:6] = integrators::rk4(&x, t, dt, &fx);

        t += dt;
    }

    // log final timestep
}

fn fx(x: &[f64; 7], t: f64) -> [f64; 7] {
    return [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0];
}
