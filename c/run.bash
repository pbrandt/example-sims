#!/usr/bin/env bash

set -e

echo clang -O0
clang -O0 -I. -o eor eor.c -lm 
time ./eor

echo clang -O3
clang -O3 -I. -o eor eor.c -lm 
time ./eor

echo gnu -O0
gcc -O0 -I. -o eor eor.c -lm 
time ./eor

echo gnu -O3
gcc -O3 -I. -o eor eor.c -lm 
time ./eor

# plot
gnuplot plot.gp
