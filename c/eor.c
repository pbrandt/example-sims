#include "stdio.h"
#include "vec.h"
#include "integrators.h"

#define MU_EARTH 3.9860043543609598E+05 // gravitational parameter from de431
#define J2_EARTH 17555283226.38659 // un-normalized jgm-3 km
#define G0 9.80665 // standard gravity
#define ISP 2600.0 // specific impulse
#define THRUST 1.77 // newtons 100% throttle
#define MDOT THRUST / G0 / ISP // engine mass flow rate at 100% throttle
static FILE *CSV;

/**
 * @brief Computes the derivate vector of state x at time t
 * 
 * @param x state earth-centered j2000
 * @param t ephemeris time tdb
 * @param dx derivative vector
 */
void fx(state *x, f64 t, state *dx) {

    f64 r_mag = v3norm2(x->pos);
    v3 r_unit;
    v3unit(x->pos, r_unit);

    // earth gravity point mass
    v3 grav_acc;
    v3scale(r_unit, -1 * MU_EARTH / r_mag / r_mag, grav_acc);

    // j2 term
    v3 j2_acc;
    f64 r7 = pow(r_mag, 7);
    f64 scale = J2_EARTH / r7;
    f64 x2y2 = x->pos[0] * x->pos[0] + x->pos[1] * x->pos[1];
    f64 z2 = x->pos[2] * x->pos[2];
    j2_acc[0] = scale * x->pos[0] * (6.0 * z2 - 3.0 / 2.0 * x2y2);
    j2_acc[1] = scale * x->pos[1] * (6.0 * z2 - 3.0 / 2.0 * x2y2);
    j2_acc[2] = scale * x->pos[2] * (3.0 * z2 - 9.0 / 2.0 * x2y2);

    // electric thrust
    v3 thrust_acc;
    v3unit(x->vel, thrust_acc);
    v3scale(thrust_acc, THRUST / 1.0e3 / x->mass, thrust_acc);

    for (i32 i = 0; i < 3; i++) {
        dx->pos[i] = x->vel[i];
        dx->vel[i] = grav_acc[i] + j2_acc[i] + thrust_acc[i];
    }

    dx->mass = -1 * MDOT;
}

/**
 * @brief Writes data to the csv
 * 
 * @param x state earth j2000
 * @param t ephemeris time tdb
 */
void log_csv(state *x, f64 t) {
    fprintf(CSV, "%f, %f, %f, %f, %f, %f, %f, %f, %f, %f\n",
        t,
        x->pos[0], x->pos[1], x->pos[2],
        x->vel[0], x->vel[1], x->vel[2],
        x->mass, t / 86400.0, v3norm2(x->pos)
    );
}

int main() {
    printf("starting simulation\n");
    
    // starting state in low earth orbit
    state x = {
        .pos = { 4356.814418455364, -5192.249235460436, 1.169076583991219e-12 },
        .vel = { 6.98729537277121, 5.863036970219034, -4.849863326963697 },
        .mass = 18000.0
    };
    f64 t = 0.0; // start at ephemeris time epoch
    f64 tf = 86400.0 * 100; // propagate for 100 days
    const f64 dt = 1; // integration timestep is 1 second (because i'm testing how fast the code is)
    const f64 log_interval = 300.0; // log every 300 seconds or five minutes
    f64 t_next_log = 0.0; // next timestep to log data in

    CSV = fopen("log.csv", "w+");
    fprintf(CSV, "t {s}, x {km}, y {km}, z {km}, vx {km/s}, vy {km/s}, vz {km/s}, m {kg}, met {day}, r {km}\n");

    while (t < tf) {
        // log at beginning of step
        if (t >= t_next_log) {
            log_csv(&x, t);
            t_next_log += log_interval;
        }

        // single rk4 step nothing fancy
        rk4(&x, t, dt, &fx, &x);

        t += dt;
    }

    // log final state
    log_csv(&x, t);

    return 0;
}
