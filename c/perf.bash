#!/usr/bin/env bash

# check that perf is installed
if ! type perf >/dev/null 2>/dev/null; then
    echo error: "perf" tool not found.
    echo install perf, e.g. sudo apt install linux-tools-$(uname -r) linux-tools-generic
    exit 420
fi

# allow perf to record interesting stuff for now
echo -1 | sudo tee /proc/sys/kernel/perf_event_paranoid

# compile unoptimized
clang -O0 -I. -lm -o eor-perf eor.c

# start process
./eor-perf &

# record performance data
perf record -F 99 -p $! -g &
PERF_PID=$!
sleep 2
kill $PERF_PID
sleep 1
perf script > out.perf

# print flame chart
FLAME_HOME='../flamegraph'
$FLAME_HOME/stackcollapse-perf.pl out.perf > out.folded
$FLAME_HOME/flamegraph.pl out.folded > flame.svg

# set perf settings back to safe
echo 4 | sudo tee /proc/sys/kernel/perf_event_paranoid

