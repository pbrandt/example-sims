#ifndef INTEGRATORS_H_DEFINED
#define INTEGRATORS_H_DEFINED

#include <stdint.h>
#include "vec.h"

static const u32 STATE_LEN = 7u;
typedef struct _state_t {
    v3 pos;
    v3 vel;
    f64 mass;
} state;

/**
 * @brief Derivative function type. Params: state, time, output
 * 
 */
typedef void (*derivative)(state *x, f64 t, state *dx);

/**
 * @brief add two state vectors together
 * 
 * @param x state x
 * @param y state y
 * @param out 
 * @return state* 
 */
static inline state* s_add(state* x, state* y, state* out) {
    return (state*)add((f64*)x, (f64*)y, STATE_LEN, (f64*)out);
}

/**
 * @brief scale a state vector by a constant value
 * 
 * @param x state
 * @param c constant
 * @param out 
 * @return state* 
 */
static inline state* s_scale(state* x, f64 c, state* out) {
    return (state*)scale((f64*)x, c, STATE_LEN, (f64*)out);
}

/**
 * @brief Perform one step of the Runge-Kutta 4th order integrator algorithm
 * 
 * @param x state vector to integrate
 * @param t current time
 * @param dt timestep size
 * @param fx derivative function
 * @param x_next output
 * @return state* output
 */
static inline state* rk4(state* x, f64 t, f64 dt, derivative fx, state* x_next) {
    state k1, k2, k3, k4, temp, out;

    (*fx)(x, t, &k1);
    (*fx)(s_add(x, s_scale(&k1, dt/2.0, &temp), &temp), t + dt/2.0, &k2);
    (*fx)(s_add(x, s_scale(&k2, dt/2.0, &temp), &temp), t + dt/2.0, &k3);
    (*fx)(s_add(x, s_scale(&k3, dt, &temp), &temp), t + dt, &k4);

    s_add(x, s_scale(&k1, dt/6.0, &temp), &out);
    s_add(&out, s_scale(&k2, dt/3.0, &temp), &out);
    s_add(&out, s_scale(&k3, dt/3.0, &temp), &out);
    s_add(&out, s_scale(&k4, dt/6.0, &temp), x_next);

    return x_next;
}

#endif