#ifndef VEC_H_DEFINED
#define VEC_H_DEFINED

#include "stdlib.h"
#include "string.h"
#include "math.h"
#include "stdint.h"


typedef double f64;
typedef int32_t i32;
typedef uint32_t u32;
typedef f64 v3[3];
typedef f64* v3p;

/**
 * @brief Scale a vector by a constant
 * 
 * @param vec vector of any length
 * @param c constant
 * @param len length of vector
 * @param out vector of length len
 * @return f64* out pointer for chaining
 */
static inline f64* scale(f64* vec, f64 c, u32 len, f64* out) {
    for (int i = 0; i < len; i++) {
        out[i] = c * vec[i];
    }
    return out;
};

/**
 * @brief Scale a cartesian vector by a constant
 * 
 * @param vec cartesian vector
 * @param c constant
 * @param out cartesian vector
 * @return v3p out pointer for chaining
 */
static inline v3p v3scale(v3 vec, f64 c, v3 out) {
    return (v3p)scale((f64*)vec, c, 3, (f64*)out);
}

/**
 * @brief Dot product of two vectors of arbitrary length
 * 
 * @param a vector of any length
 * @param b vector of any length
 * @param len length
 * @return f64 dot product
 */
static inline f64 dot(f64* a, f64* b, u32 len) {
    f64 sum = 0;
    for (u32 i = 0; i < len; i++) {
        sum = sum + a[i] * b[i];
    }
    return sum;
}

/**
 * @brief Dot product of two cartesian vectors
 * 
 * @param a cartesian vector
 * @param b cartesian vector
 * @return f64 dot product
 */
static inline f64 v3dot(v3 a, v3 b) {
    return dot((f64*)a, (f64*)b, 3);
}

/**
 * @brief Add two vectors of arbitrary length, element by element
 * 
 * @param a vector of any length
 * @param b vector of any length
 * @param len length
 * @param out summed vector
 * @return f64* out vector for chaining
 */
static inline f64* add(f64* a, f64* b, u32 len, f64* out) {
    for (int i = 0; i < len; i++) {
        out[i] = a[i] + b[i];
    }
    return out;
}

/**
 * @brief Add two cartesian vectors, element by element
 * 
 * @param a cartesian vector
 * @param b cartesian vector
 * @param out cartesian vector
 * @return v3p out for chaining
 */
static inline v3p v3add(v3 a, v3 b, v3 out) {
    return (v3p)add((f64*)a, (f64*)b, 3, (f64*)out);
}


/**
 * @brief Add a constant to a vector of arbitrary length
 * 
 * @param vec vector of any length
 * @param c constant
 * @param len length
 * @param out summed vector
 * @return f64* out vector for chaining
 */
static inline f64* addconst(f64* vec, f64 c, u32 len, f64* out) {
    for (int i = 0; i < len; i++) {
        out[i] = vec[i] + c;
    }
    return out;
}

/**
 * @brief Add a constant to a cartesian vector
 * 
 * @param vec cartesian vector
 * @param c cartesian vector
 * @param out cartesian vector
 * @return v3p out for chaining
 */
static inline v3p v3addconst(v3 vec, f64 c, v3 out) {
    return (v3p)addconst((f64*)vec, c, 3, (f64*)out);
}

/**
 * @brief L1 norm (sum of elements) for a vector of arbitrary length
 * 
 * @param vec vector of arbitrary length
 * @param len length
 * @return f64 L1 norm
 */
static inline f64 norm1(f64* vec, u32 len) {
    f64 sum = 0;
    for (u32 i = 0; i < len; i++) {
        sum = sum + vec[i];
    }
    return sum;
}

/**
 * @brief L2 norm of a vector of arbitrary length
 * 
 * @param vec vector of arbitrary length
 * @param len length
 * @return f64 L2 norm
 */
static inline f64 norm2(f64* vec, u32 len) {
    return sqrt(dot(vec, vec, len));
}

/**
 * @brief L2 norm of cartesian vector
 * 
 * @param vec cartesian vector
 * @return f64 L2 norm
 */
static inline f64 v3norm2(v3 vec) {
    return norm2((f64*)vec, 3);
}

/**
 * @brief Unit vector of arbitrary length
 * 
 * @param vec vector of arbitrary length 
 * @param len length
 * @param out unit vector
 * @return f64* unit vector for chaining
 */
static inline f64* unit(f64* vec, u32 len, f64* out) {
    f64 n = norm2(vec, len);
    for (int i = 0; i < len; i++) {
        out[i] = vec[i] / n;
    }
    return out;
}

/**
 * @brief Unit cartesian vector
 * 
 * @param vec cartesian vector
 * @param out unit vector
 * @return v3p unit vector for chaining
 */
static inline v3p v3unit(v3 vec, v3 out) {
    return (v3p)unit((f64*)vec, 3, (f64*)out);
}

#endif