#!/usr/bin/env bash

set -e

fpc -g vec.p
fpc -g rk4.p
fpc -g eor.p
time ./eor
gnuplot plot.gp

