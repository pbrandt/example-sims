# Example sims

Comparing several different implementations of a 100 day low-thrust spiral out in earth orbit.

|                       |             |
| --------------------- | ----------- |
| Force Model           | J2 Earth    |
| Integrator            | RK4         |
| Integration timestep  | 1 s         |
| Logging interval      | 5 min       |
| Sim duration          | 100 days    |
| Initial mass          | 18,000 kg   |
| Thrust                | 1.77 N      |
| Isp                   | 2600 s      |
| Thrust pointing (VUW) | (1, 0, 0)   |
| Solar array pointing  | Not modeled |


## Preliminary Performance Metrics

|                 | -O0       | -O3      | Notes                          |
| --------------- | --------- | -------- | ------------------------------ |
| Fortran - gnu   | 0m3.181s  | 0m1.574s | gfortran -Ofast instead of -O3 |
| Fortran - intel | 0m4.354s  | 0m1.489s |                                |
| C - clang       | 0m5.842s  | 0m0.926s |                                |
| C - gnu         | 0m7.399s  | 0m1.395s |                                |
| C++ - clang     | 0m11.820s | 0m1.054s |                                |
| C++ - gnu       | 0m10.773s | 0m1.682s |                                |
| Pascal          | 0m9.844s  |          | need to get rid of allocs      |
| Go              | 0m4.633s  |          |                                |
| Node.js         | 1m4.098s  | N/A      | need to do more in-place stuff |
| Julia           | 1m21.695s | N/A      | need to get rid of allocs      |
| Python          | 10m...    |          |                                |

## Profiling

https://github.com/brendangregg/FlameGraph
