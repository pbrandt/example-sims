using Printf
using LinearAlgebra

include("rk4.jl")

const GRAVITATIONAL_CONSTANT = 6.674e-11 / 1e9 # uses km as the base unit
const THRUST_LEVEL = 1.77 # newtons
const ISP = 2600

function f(x, t)
    # for convenience split out the state
    local r = view(x, 1:3)
    local v = view(x, 4:6)
    local m = x[7]

    # calculate gravitational force
    local r_earth = sqrt(dot(r, r))
    local earth_gravity_magnitude = GRAVITATIONAL_CONSTANT * 5.9722e24 / r_earth / r_earth
    local earth_gravity_vector =   -1 * earth_gravity_magnitude * r / r_earth

    # calculate spacecraft thrust
    local v_unit = v / sqrt(dot(v, v))
    local thrust_acc_km = THRUST_LEVEL / m / 1e3 * v_unit
    local thrust_mdot = -1 * THRUST_LEVEL / 9.80665 / ISP
    local dv = earth_gravity_vector + thrust_acc_km
    local dr = v
    local dm = thrust_mdot

    return [dr; dv; dm]

end

function main()
    local t::Float64 = 0
    local x = [
        4356.814418455364, # km
        -5192.249235460436,
        1.169076583991219e-12,
        6.98729537277121, # km/s
        5.863036970219034,
        -4.849863326963697,
        18000.0 # kg
    ]
    local dt::Float64 = 1
    local tf::Float64 = 100 * 86400

    # set up the log file for the data with a header line
    local log = open("log.csv", "w")
    write(log, "t {s}, x {km}, y {km}, z {km}, vx {km/s}, vy {km/s}, vz {km/s}, m {kg}, met {day}, r {km}\n")

    println("starting julia sim")
    println("ps. julia is not really a functional language")
    println("    maybe it could be, but this code is procedural")

    while t <= tf
        if t % 300 == 0
            local met_days::Float64 = t / 86400
            local r_vec = view(x, 1:3)
            local r = sqrt(dot(r_vec, r_vec))
            @printf(log, "%d,", t)
            join(log,  x, ',')
            @printf(log, ",%f,%f\n", met_days, r)
        end

        if t % 86400 == 0
            println(t / 86400)
        end
        
        x = RK4.integrate(x, t, f, dt)
        t = t + dt
    end

    println("done")
end

main()