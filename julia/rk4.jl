module RK4

function integrate(x0, t0, f, dt)
    local k1 = f(x0, t0)
    local k2 = f(x0 + k1 * dt / 2, t0 + dt / 2)
    local k3 = f(x0 + k2 * dt / 2, t0 + dt / 2)
    local k4 = f(x0 + k3 * dt, t0 + dt)

    local x1 = x0 + dt * (k1 / 6 + k2 / 3 + k3 / 3 + k4 / 6)
    return x1
end

end
