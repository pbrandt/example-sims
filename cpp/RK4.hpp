#ifndef _rk4_h
#define _rk4_h

#include "Vec.hpp"

/**
 * @brief performs a single step of the runge-kutta 4th order integrator
 * 
 * @tparam N state vector length
 * @param x state vector
 * @param fx derivative computation routine
 * @param t current time
 * @param dt timestep size
 * @return Vec<N>& 
 */
template<size_t N>
static inline Vec<N> rk4_step(const Vec<N>& x, Vec<N> (*fx)(const Vec<N>&, const double), const double t, const double dt) {
    Vec<N> k1, k2, k3, k4;

    k1 = (*fx)(x, t);
    k2 = (*fx)(x + k1 * dt/2.0, t + dt/2.0);
    k3 = (*fx)(x + k2 * dt/2.0, t + dt/2.0);
    k4 = (*fx)(x + k3 * dt, t + dt);

    return x + (k1 + k4) * dt / 6 + (k2 + k3) * dt / 3;
}

#endif