set grid
set key autotitle columnhead
set datafile separator ","
set terminal pngcairo dashed size 800,600 enhanced background rgb "white" linewidth 1 font "Public Sans,14"


# 3d plot
set title "Low thrust orbit - a 3d view"
set xlabel "x (km)"
set ylabel "y (km)"
set zlabel "z (km)"
set output "plot_3d.png"
splot "log.csv" using 2:3:4 with lines notitle

# xy plot
set title "Low thrust orbit - XY plane"
set output "plot_xy.png"
plot "log.csv" using 2:3 with lines notitle

# radius v time
set title "Low thrust orbit - radius"
set xlabel "mission elapsed time (days)"
set ylabel "radius (km)"
set output "plot_r.png"
set xrange [0:0.2]
plot "log.csv" u 9:10 with lines title "RKDP"

# mass v time
set title "Low thrust orbit - mass"
set ylabel "mass (kg)"
set output "plot_m.png"
plot "log.csv" u 9:8 with lines notitle
