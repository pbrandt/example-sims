#!/usr/bin/env bash

# check that perf is installed
if ! type perf >/dev/null 2>/dev/null; then
    echo error: "perf" tool not found.
    echo install perf, e.g. sudo apt install linux-tools-$(uname -r) linux-tools-generic
    exit 420
fi

# allow perf to record interesting stuff for now
echo -1 | sudo tee /proc/sys/kernel/perf_event_paranoid

# compile unoptimized if i'm trying to make the debug build faster
clang++ -O0 -std=c++98 eor.cpp -I. -o eor-perf

# start process
./eor-perf &

# record performance data (sudo apt install linux-tools-common)
perf record -F 99 -p $! -g &
PERF_PID=$!
sleep 2
kill $PERF_PID
wait $PERF_PID
sleep 1
perf script > out.perf

# print flame chart
FLAME_HOME='../flamegraph'
$FLAME_HOME/stackcollapse-perf.pl out.perf > out.folded
$FLAME_HOME/flamegraph.pl out.folded > flame.svg

# return the perf setting back to a safe one
echo 4 | sudo tee /proc/sys/kernel/perf_event_paranoid
