#include <stdio.h>

#include "Vec.hpp"
#include "RK4.hpp"

static const double MU_EARTH = 3.9860043543609598E+05; // gravitational parameter from de431
static const double J2_EARTH = 17555283226.38659; // un-normalized jgm-3 km
static const double ISP = 2600.0; // specific impulse
static const double THRUST = 1.77; // newtons 100% throttle
static const double G0 = 9.80665; // standard gravity
static const double MDOT = THRUST / G0 / ISP; // engine mass flow rate at 100% throttle
static FILE *CSV;

struct State {
    v3 pos;
    v3 vel;
    double mass;
};

/**
 * @brief Computes the derivatives of state x at time t
 * 
 * @param x state
 * @param t ephemeris time tdb
 * @return State 
 */
inline State state_deriv(const State& x, const double t) {
    // compute gravitation acceleration for point mass earth
    // units km/s/s
    double r = x.pos.mag();
    v3 r_unit = x.pos.unit();
    v3 gravity_acc = -1.0 * r_unit * MU_EARTH / r / r;

    // compute J2 term from un-normalized coefficient
    double r7 = pow(r, 7);
    double scale = J2_EARTH / r7;
    double x2y2 = x.pos[0] * x.pos[0] + x.pos[1] * x.pos[1];
    double z2 = x.pos[2] * x.pos[2];
    v3 j2_acc = {
        scale * x.pos[0] * (6.0 * z2 - 3.0 / 2.0 * x2y2),
        scale * x.pos[1] * (6.0 * z2 - 3.0 / 2.0 * x2y2),
        scale * x.pos[2] * (3.0 * z2 - 9.0 / 2.0 * x2y2)
    };

    // thrust along the velocity vector
    // units km/s/s
    v3 thrust_acc = x.vel.unit() * THRUST / 1e3 / x.mass;

    // sum the derivatives
    State deriv;
    deriv.pos = x.vel;
    deriv.vel = gravity_acc + j2_acc + thrust_acc;
    deriv.mass = -1 * MDOT;

    return deriv;
}

/**
 * @brief Alias for state_deriv
 * 
 * @param x e.g. reinterpret_cast<Vec<7>&>(some state)
 * @param t 
 * @return Vec<7> 
 */
inline Vec<7> state_deriv(const Vec<7>& x, const double t) {
    const State& x_as_state = reinterpret_cast<const State&>(x);
    State deriv = state_deriv(x_as_state, t);
    return reinterpret_cast<Vec<7>&>(deriv);
}

/**
 * @brief logs data to the csv for plotting later
 * 
 * @param x State
 * @param t ephemeris time tdb
 */
void log_csv(State& x, double t) {
    fprintf(CSV, "%f, %f, %f, %f, %f, %f, %f, %f, %f, %f\n",
        t,
        x.pos[0], x.pos[1], x.pos[2],
        x.vel[0], x.vel[1], x.vel[2],
        x.mass, t / 86400.0, x.pos.mag()
    );
}

int main() {
    printf("starting simulation\n");

    // Initialize spacecraft in something resembling GTO, Earth J2000 coordinates
    State x;
    x.pos[0] = 4356.814418455364;
    x.pos[1] = -5192.249235460436;
    x.pos[2] = 1.169076583991219e-12;
    x.vel[0] = 6.98729537277121;
    x.vel[1] = 5.863036970219034;
    x.vel[2] = -4.849863326963697;
    x.mass = 18000;

    // a vector reference to the state, for RK4 propagation
    Vec<7>& x_vec = reinterpret_cast<Vec<7>&>(x);

    // Start at Jan 1 2000 for fun
    double t = 0.0;

    // Run sim for 100 days
    const double tf = 86400.0 * 100.0;

    // 1 second timestep because i'm trying to strain the system
    const double dt = 1.0;
    const double log_interval = 300.0;
    double next_log = 0.0;
    CSV = fopen("log.csv", "w+");
    fprintf(CSV, "t {s}, x {km}, y {km}, z {km}, vx {km/s}, vy {km/s}, vz {km/s}, m {kg}, met {day}, r {km}\n");

    // run the rk4 integrator until final time
    while (t < tf) {
        // log at beginning of step
        if (t >= next_log) {
            log_csv(x, t);
            next_log += log_interval;
        }

        // single rk4 step nothing fancy
        x_vec = rk4_step(x_vec, &state_deriv, t, dt);

        t += dt;
    }

    // log the final state
    log_csv(x, t);

    return 0;
}
