#ifndef _vec_h
#define _vec_h

#include <math.h>

/**
 * @brief Floating point Vec of the specified length. This is for math, not for
 * object-oriented list-like operations (e.g. concatenation, sorting, etc)
 * 
 * @tparam N length of Vec
 */
template<size_t N>
class Vec {
    public:
    double data[N];

    /**
     * @brief Retrieve a reference to the value at a given index
     * 
     * @param i 
     * @return double& 
     */
    double& operator[](size_t i) {
        if (i >= N) {
            // it sure is too bad C++ doesn't provide a mechanism for COMPILE TIME ERRORS here :P
        }
        return data[i];
    }

    /**
     * @brief Retrieve a const reference to the value at a given index
     * 
     * @param i 
     * @return const double& 
     */
    const double& operator[](size_t i) const {
        if (i >= N) {
            // it sure is too bad C++ doesn't provide a mechanism for COMPILE TIME ERRORS here :P
        }
        return data[i];
    }

    /**
     * @brief add a vector of the same size to this one, element by element
     * 
     * @param rhs 
     * @return Vec<N>& 
     */
    Vec<N>& operator+=(const Vec<N>& rhs) {
        for (size_t i = 0; i < N; i++) {
            data[i] += rhs.data[i];
        }
        return *this;
    }

    /**
     * @brief subtract another vector from this one, element by element
     * 
     * @param rhs 
     * @return Vec<N>& 
     */
    Vec<N>& operator-=(const Vec<N>& rhs) {
        for (size_t i = 0; i < N; i++) {
            data[i] -= rhs.data[i];
        }
        return *this;
    }

    /**
     * @brief Scale this vector by a multiplier
     * 
     * @param rhs 
     * @return Vec<N>& 
     */
    Vec<N>& operator*=(const double rhs) {
        for (size_t i = 0; i < N; i++) {
            data[i] *= rhs;
        }
        return *this;
    }

    /**
     * @brief Divide this vector by a divisor, element by element
     * 
     * @param rhs 
     * @return Vec<N>& 
     */
    Vec<N>& operator/=(const double rhs) {
        for (size_t i = 0; i < N; i++) {
            data[i] /= rhs;
        }
        return *this;
    }

    /**
     * @brief Add two vectors together. The "friend" allows the copmiler to
     * better optimize chains of additions
     * 
     * @param lhs 
     * @param rhs 
     * @return Vec<N> 
    */
    friend Vec<N> operator+(Vec<N> lhs, const Vec<N>& rhs) {
        lhs += rhs;
        return lhs;
    }

    /**
     * @brief Subtract two vectors.
     * 
     * @param lhs 
     * @param rhs 
     * @return Vec<N> 
     */
    friend Vec<N> operator-(Vec<N> lhs, const Vec<N>& rhs) {
        lhs -= rhs;
        return lhs;
    }

    /**
     * @brief Scale a vector
     * 
     * @param lhs 
     * @param rhs 
     * @return Vec<N> 
     */
    friend Vec<N> operator*(Vec<N> lhs, const double rhs) {
        lhs *= rhs;
        return lhs;
    }

    /**
     * @brief Scale a vector via division by constant
     * 
     * @param lhs 
     * @param rhs 
     * @return Vec<N> 
     */
    friend Vec<N> operator/(Vec<N> lhs, const double rhs) {
        lhs /= rhs;
        return lhs;
    }

    /**
     * @brief Scale a vector. The opposite rhs/lhs from above.
     * 
     * @param lhs 
     * @param rhs 
     * @return Vec<N> 
    */
    friend Vec<N> operator*(const double lhs, Vec<N> rhs) {
        rhs *= lhs;
        return rhs;
    }

    /**
     * @brief Scale a vector via division. The opposite rhs/lhs.
     * 
     * @param lhs 
     * @param rhs 
     * @return Vec<N> 
     */

    friend Vec<N> operator/(const double lhs, Vec<N> rhs) {
        rhs /= lhs;
        return rhs;
    }
    /**
     * @brief Add a constant to a vector, element by element.
     * Honestly this one seems sketch to me, could hide some bugs.
     * 
     * @param lhs 
     * @param rhs 
     * @return Vec<N> 
     */
    friend Vec<N> operator+(const double lhs, Vec<N> rhs) {
        rhs += lhs;
        return rhs;
    }

    /**
     * @brief Subtract a constant from a vector, element by element.
     * Sketch, like addition above.
     * 
     * @param lhs 
     * @param rhs 
     * @return Vec<N> 
     */
    friend Vec<N> operator-(const double lhs, Vec<N> rhs) {
        rhs -= lhs;
        return rhs;
    }

    /**
     * @brief magnitude or L2 norm of the vector
     * 
     * @return double 
     */
    inline double mag() const {
        double sum = 0.0;
        for (size_t i = 0; i < N; i++) {
            sum = sum + data[i]*data[i];
        }
        return sqrt(sum);
    }

    /**
     * @brief Return a unit-normalized vector
     * 
     * @return Vec<N> 
     */
    inline Vec<N> unit() const {
        return *this / this->mag();
    }
};

/**
 * @brief Dot product of two Vecs of the same length
 * 
 * @tparam N Vec length
 * @param lhs 
 * @param rhs 
 * @return double 
 */
template <size_t N>
static inline double operator*(const Vec<N>& lhs, const Vec<N>& rhs) {
    double sum = 0;
    for (size_t i = 0; i < N; i++) {
        sum += lhs[i] * rhs[i];
    }
    return sum;
}

/**
 * @brief Convenience definition for a standard cartesian three element vector
 * 
 */
typedef Vec<3> v3;

#endif