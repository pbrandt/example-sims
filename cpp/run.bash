#!/usr/bin/env bash

set -e

echo clang++ -O0
clang++ -O0 -std=c++98 eor.cpp -I. -o eor
time ./eor

echo clang++ -O3
clang++ -O3 -std=c++98 eor.cpp -I. -o eor
time ./eor

echo g++ -O0
g++ -O0 -std=c++98 eor.cpp -I. -o eor
time ./eor

echo g++ -O3
g++ -O3 -std=c++98 eor.cpp -I. -o eor
time ./eor

# plot
gnuplot plot.gp
