package com.pbrandt;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.lang.Math;

public class Vector {
    public double data[];
    public int size;

    public Vector(int size) {
        data = new double[size];
        this.size = size;
    }

    public Vector(double[] arr) {
        data = arr;
        this.size = arr.length;
    }

    public double mag() {
        double sum = 0;
        for (int i = 0; i < size; i++) {
            sum = sum + data[i]*data[i];
        }
        return Math.sqrt(sum);
    }

    public Vector unit() {
        return div(mag());
    }

    public Vector div(double denominator) {
        Vector result = copy();
        for (int i = 0; i < size; i++) {
            result.data[i] = result.data[i] / denominator;
        }
        return result;
    }

    public Vector copy() {
        return new Vector(data);
    }
}