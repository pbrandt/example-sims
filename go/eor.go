package main

import (
	"bufio"
	"fmt"
	"os"
	"math"
)

func fx(x []float64, t float64, dx []float64) []float64 {
	// alias the states
	r := x[0:3] // km
	v := x[3:6] // km/s
	m := x[6]   // kg

	// position deriviative is the velocity
	copy(dx[0:3], v)

	// earth gravity point mass
	r_mag := Norm(r)
	g := 3.9860043543609598E+05 / r_mag / r_mag
	var gravity_acc [3]float64
	Mul(-1*g, Unit(r, gravity_acc[:]), gravity_acc[:])

	// j2
	scale := 17555283226.38659 / math.Pow(r_mag, 7)
	x2y2 := x[0] * x[0] + x[1] * x[1]
	z2 := x[2] * x[2]
	var j2_acc [3]float64
	j2_acc[0] = scale * x[0] * (6.0 * z2 - 3.0 / 2.0 * x2y2)
	j2_acc[1] = scale * x[1] * (6.0 * z2 - 3.0 / 2.0 * x2y2)
	j2_acc[2] = scale * x[2] * (3.0 * z2 - 9.0 / 2.0 * x2y2)

	// spacecraft solar electric propulsion
	THRUST := 1.77
	ISP := 2600.0
	var thrust_acc [3]float64
	Mul(THRUST/m/1e3, Unit(v, thrust_acc[:]), thrust_acc[:])
	sep_mdot := THRUST / 9.80665 / ISP

	for i := range gravity_acc {
		dx[i+3] = gravity_acc[i] + j2_acc[i] + thrust_acc[i]
	}
	dx[6] = -1 * sep_mdot
	return dx
}

func main() {
	// pos (km), vel (km), mass (kg)
	x0 := [7]float64{4356.814418455364, -5192.249235460436, 1.169076583991219e-12, 6.98729537277121, 5.863036970219034, -4.849863326963697, 18000.0}
	x := x0[:]

	var t float64 = 0
	var tf float64 = 86400 * 100
	var dt float64 = 1

	// create log file
	f, err := os.Create("./log.csv")
	if err != nil {
		panic(err)
	}
	w := bufio.NewWriter(f)
	w.WriteString("t {s}, x {km}, y {km}, z {km}, vx {km/s}, vy {km/s}, vz {km/s}, m {kg}, met {day}, r {km}\n")

	fmt.Println("starting simulation")
	for t <= tf {
		if int(t)%300 == 0 {
			// log to file
			met_days := t / 86400
			r := Norm(x[0:3])
			w.WriteString(fmt.Sprintf("%-1g, %-1g, %-1g, %-1g, %-1g, %-1g, %-1g, %-1g, %-1g, %-1g\n", t, x[0], x[1], x[2], x[3], x[4], x[5], x[6], met_days, r))
		}

		x = Integrate(x, t, fx, dt, x)
		t = t + dt
	}

	fmt.Println("done")
	w.Flush()
}
