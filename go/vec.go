package main

import (
	"math"
)

func Add(v1 []float64, v2 []float64, v_out []float64) []float64 {
	for i, v := range v1 {
		v_out[i] = v + v2[i]
	}
	return v_out
}

func Mul(c float64, v []float64, v_out []float64) []float64 {
	for i, x := range v {
		v_out[i] = c * x
	}
	return v_out
}

func Norm(v []float64) float64 {
	norm := 0.0
	for _, x := range v {
		norm = norm + x*x
	}
	return math.Sqrt(norm)
}

func Unit(v []float64, out []float64) []float64 {
	norm := Norm(v)
	return Mul(1/norm, v, out)
}

func Concatenate(vecs ...[]float64) []float64 {
	total_length := 0
	for _, vec := range vecs {
		total_length = total_length + len(vec)
	}

	out_vec := make([]float64, total_length)
	i := 0
	for _, vec := range vecs {
		for _, x := range vec {
			out_vec[i] = x
			i = i + 1
		}
	}

	return out_vec
}
