package main

type derivative func(x []float64, t float64, dx []float64) []float64

func Integrate(x0 []float64, t0 float64, f derivative, dt float64, x_out []float64) []float64 {
	temp := make([]float64, len(x0))
	k1 := make([]float64, len(x0))
	k2 := make([]float64, len(x0))
	k3 := make([]float64, len(x0))
	k4 := make([]float64, len(x0))

	f(x0, t0, k1)
	f(Add(x0, Mul(dt/2, k1, temp), temp), t0+dt/2, k2)
	f(Add(x0, Mul(dt/2, k2, temp), temp), t0+dt/2, k3)
	f(Add(x0, Mul(dt, k3, temp), temp), t0+dt, k4)

	Add(x0, Mul(dt/6, k1, temp), x_out)
	Add(x_out, Mul(dt/3, k2, temp), x_out)
	Add(x_out, Mul(dt/3, k3, temp), x_out)
	Add(x_out, Mul(dt/6, k4, temp), x_out)

	return x_out
}
