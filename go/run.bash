#!/usr/bin/env bash

set -e

go build eor.go vec.go rk4.go

time ./eor

gnuplot plot.gp

