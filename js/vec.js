
function add(v1, v2) {
    return v1.map((v, i) => v + v2[i])
}

function mul(c, v) {
    return v.map(x => x * c)
}

function norm(v) {
    return Math.sqrt(v.reduce((sum, x) => sum + x*x, 0))
}

function unit(v) {
    return mul(1 / norm(v), v)
}

function concatenate(...vectors) {
    return vectors.reduce((all, v) => {
        return all.concat(v)
    }, [])
}

module.exports = {
    add,
    mul,
    norm,
    unit,
    concatenate
}