const vec = require('./vec')

function integrate(x0, t0, f, dt) {
    const k1 = f(x0, t0)
    const k2 = f(vec.add(x0, vec.mul(dt/2, k1)), t0 + dt/2)
    const k3 = f(vec.add(x0, vec.mul(dt/2, k2)), t0 + dt/2)
    const k4 = f(vec.add(x0, vec.mul(dt, k3)), t0 + dt)

    var x1 = vec.add(x0, vec.mul(dt/6, k1))
    x1 = vec.add(x1, vec.mul(dt/3, k2))
    x1 = vec.add(x1, vec.mul(dt/3, k3))
    x1 = vec.add(x1, vec.mul(dt/6, k4))
    return x1
}

module.exports = {
    integrate
}
