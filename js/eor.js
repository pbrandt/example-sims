const rk4 = require('./rk4')
const vec = require('./vec')
const fs = require('fs')

const GRAVITATIONAL_CONSTANT = 6.674e-11 / 1e9 // using km as the base unit

/**
 * Calculate the derivative of state x at time t
 * The state is earth-centered J2000 position (km), velocity (km/s), and mass (kg) as the seventh element
 * @param {Array} x state
 * @param {Number} t time
 */
function f(x, t) {
    // for convenience, split out the state
    const r = x.slice(0, 3) // km
    const v = x.slice(3, 6) // km/s
    const m = x[6] // kg

    // position derivative is the velocity
    const dr = v

    // we will build up the accelerations
    var dv = [0, 0, 0]

    // dm depends on thrust / outgassing / venting
    var dm = 0

    // Calculate earth gravity magnitude
    const r_earth = vec.norm(r)
    const earth_gravity_magnitude = GRAVITATIONAL_CONSTANT * 5.9722e24 / r_earth / r_earth
    const earth_gravity_vector = vec.mul(-1 * earth_gravity_magnitude, vec.unit(r))
    dv = vec.add(dv, earth_gravity_vector)

    // add spacecraft thrust in the velocity direction
    const THRUST_LEVEL = 1.77; // newtons
    const ISP = 2600;
    const v_unit = vec.unit(v)
    const thrust_acc_km = vec.mul(THRUST_LEVEL / m / 1e3, v_unit)
    const thrust_mdot = THRUST_LEVEL / 9.80665 / ISP
    dv = vec.add(dv, thrust_acc_km)
    dm = dm - thrust_mdot

    return vec.concatenate(dr, dv, [dm])
}

// Main program
// propagate a spacecraft out for 100 days
// log state every 300 seconds
// dt is 1 second
var t = 0
var x = [
    4356.814418455364, // km
    -5192.249235460436,
    1.169076583991219e-12,
    6.98729537277121, // km/s
    5.863036970219034,
    -4.849863326963697,
    18000 // kg
]
const tf = 100 * 86400
const dt = 1

// set up log file
const log = fs.createWriteStream('log.csv')
log.write('t {s}, x {km}, y {km}, z {km}, vx {km/s}, vy {km/s}, vz {km/s}, m {kg}, met {day}, r {km}\n')

console.log('starting')
while (t <= tf) {
    // log at intervals
    if (t % 300 == 0) {
        const met_days = t / 86400
        const r = vec.norm(x.slice(0, 3))
        log.write(vec.concatenate([t], x, [met_days, r]).join(', ') + '\n')
    }

    // update user on progress
    if (t % 86400 == 0) {
        console.log('day', t / 86400)
    }

    // propagate state
    x = rk4.integrate(x, t, f, dt)
    t = t + dt
}
console.log('done')