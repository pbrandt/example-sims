const vec = require('./vec')
const fs = require('fs')

var rkdp54_factory = function () {
    return {
        step(dt) {
            console.log('taking step of size ' + dt)


        }
    }
}

function test() {
    var integrator = rkdp54_factory();
    var t = 0
    var x = [
        4356.814418455364, // km
        -5192.249235460436,
        1.169076583991219e-12,
        6.98729537277121, // km/s
        5.863036970219034,
        -4.849863326963697,
        18000 // kg
    ]
    const GRAVITATIONAL_CONSTANT = 6.674e-11 / 1e9 // using km as the base unit

    // Derivative function
    var fx = function (x, t) {
        const r = x.slice(0, 3) // km
        const v = x.slice(3, 6) // km/s
        const m = x[6] // kg

        // position derivative is the velocity
        const dr = v
    
        // we will build up the accelerations
        var dv = [0, 0, 0]
    
        // dm depends on thrust / outgassing / venting
        var dm = 0

        // Earth gravity point mass
        const re = vec.norm(r);
        const earth_gravity_magnitude = GRAVITATIONAL_CONSTANT * 5.9722e24 / r_earth / r_earth
        const earth_gravity_vector = vec.mul(-1 * earth_gravity_magnitude, vec.unit(r))
        dv = vec.add(dv, earth_gravity_vector)

    }

    // Stopping condition
    var gx = function (x, t) {

    }
}